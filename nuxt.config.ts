// https://nuxt.com/docs/api/configuration/nuxt-config
import { resolve } from 'path';
export default defineNuxtConfig({
    alias: {
        assets: "<rootDir>/assets",
        'images': resolve(__dirname, './assets/img'),
    },
    css: [
        "/assets/styles/base.scss"
    ],
    vite: {
        css: {
            preprocessorOptions: {
                scss: {
                    additionalData: '@import "~/assets/styles/variables.scss";',
                },
            },
        },
    },
    modules: ["@nuxt/content"],
    postcss: {
        plugins: {
            autoprefixer: {}
        }
    },
    app: {
        pageTransition: { name: 'page', mode: 'out-in' }
    },
    ssr: false
})