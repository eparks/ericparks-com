function canvasTest() {
    var canvas = document.querySelector('canvas');
    var c = canvas.getContext('2d');

    // Set canvas size
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    // Boxes
    c.fillStyle = 'rgba(255,0,0,.5)';
    c.fillRect(100,100,100,100);
    c.fillStyle = 'rgba(0,255,0,.5)';
    c.fillRect(800,300,200,100);

    // Lines
    c.beginPath();
    c.moveTo(100,100);
    c.lineTo(600,100);
    c.lineTo(600,400);
    c.lineTo(1000,400);
    c.strokeStyle = "red";
    c.lineWidth = 5;
    c.stroke();

    // For loop for creating circles
    for (var i = 0; i < 200; i++) {
      var x = Math.random() * window.innerWidth;
      var y = Math.random() * window.innerHeight;

      var colors= ['aqua', 'black', 'blue', 'fuchsia', 'gray', 'green',
      'lime', 'maroon', 'navy', 'olive', 'orange', 'purple', 'red',
      'silver', 'teal', 'white', 'yellow'];
      var hue = colors[Math.floor(Math.random() * colors.length)];

      // Random size for circle radius
      // function getRandomInt(max) {
      //   return Math.floor(Math.random() * Math.floor(max));
      // }
      // var r = getRandomInt(60);

      var r = (Math.floor(Math.random() * 70));

      //console.log(r);
      //console.log(hue);

      // Arc/Circle
      c.beginPath();
      c.arc(x, y, r , 0, Math.PI * 2, false);
      c.lineWidth = 2;
      c.strokeStyle = hue;
      c.stroke();
    }
}
canvasTest();


/*
// Smooth Scroll to hashtags
// Select all links with hashes
$('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.click(function(event) {
  // On-page links
  if (
    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
    &&
    location.hostname == this.hostname
  ) {
    // Figure out element to scroll to
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    // Does a scroll target exist?
    if (target.length) {
      // Only prevent default if animation is actually gonna happen
      event.preventDefault();
      $('html, body').animate({scrollTop: target.offset().top - 300}, 1000 );
    }
  }
});
*/
