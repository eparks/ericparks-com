import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/graphic-design',
      name: 'graphicdesign',
      component: () => import('./views/GraphicDesign.vue')
    },
    {
      path: '/ui-design',
      name: 'uidesign',
      component: () => import('./views/UiDesign.vue')
    },
    {
      path: '/photography',
      name: 'photography',
      component: () => import('./views/Photography.vue')
    },
    {
      path: '/development',
      name: 'development',
      component: () => import('./views/Development.vue')
    },
    {
      path: '/web-design',
      name: 'webdesign',
      component: () => import('./views/WebDesign.vue')
    }
  ],
  scrollBehavior () {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({ x: 0, y: 0 })
      }, 800)
    })
  }
})
