// Counter example
export const useCounter = () => useState<number>('counter', () => 0)

// Color Example
export const useColor = () => useState<string>('color', () => 'pink')

// Nav open
export const navState = () => useState<boolean>('navOpen', () => false)
